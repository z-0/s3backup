package cloud.stonehouse.s3backupGCS;

import cloud.stonehouse.s3backupGCS.s3.S3List;
import com.google.cloud.storage.Blob;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TreeMap;

class Backup extends BukkitRunnable {

    private final Boolean backupInProgress;
    private final String customPrefix;
    private final s3backupGCS s3backupGCS;
    private final Player player;

    Backup(s3backupGCS s3backupGCS, Player player, String customPrefix) {
        this.customPrefix = customPrefix;
        this.s3backupGCS = s3backupGCS;
        this.player = player;

        this.backupInProgress = s3backupGCS.inProgress();

        if (!backupInProgress) {
            s3backupGCS.sendMessage(player, "Saving worlds. This will take a moment");
            Bukkit.getScheduler().callSyncMethod(s3backupGCS, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-off"));
            Bukkit.getScheduler().callSyncMethod(s3backupGCS, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all"));
            s3backupGCS.sendMessage(player, "Worlds saved");
        }
    }

    @Override
    public void run() {
        if (backupInProgress) {
            s3backupGCS.sendMessage(player, "A backup is already in progress");

        } else {
            s3backupGCS.setProgress(true);
            try {
                String dateFormat = s3backupGCS.getFileConfig().getBackupDateFormat();
                if (s3backupGCS.illegalString(dateFormat)) {
                    throw new StringFormatException("Invalid date format. Only alphanumeric characters, underscores and " +
                            "hyphens are permitted. Please check config.yml");
                }
                if (s3backupGCS.illegalPrefix(s3backupGCS.getFileConfig().getPrefix())) {
                    throw new StringFormatException("Invalid prefix format. Only alphanumeric characters, underscores, " +
                            "hyphens and forward slashes are permitted. Please check config.yml");
                }

                new HealthCheckBuilder().start(s3backupGCS).runTaskAsynchronously(s3backupGCS);
                String archiveName = customPrefix + new SimpleDateFormat(s3backupGCS.getFileConfig().getBackupDateFormat())
                        .format(new Date()) + ".zip";
                String backupDir = s3backupGCS.getFileConfig().getBackupDir();

                s3backupGCS.sendMessage(player, "Backup initiated");
                String archivePath = backupDir + File.separator + archiveName;

                s3backupGCS.archive().zipFile(player, Paths.get("").toAbsolutePath().normalize().toString(), archivePath);
                s3backupGCS.s3Put().put(archiveName);
                s3backupGCS.archive().deleteFile(archivePath);

                s3backupGCS.sendMessage(player, "Backup complete");

                int maxBackups = s3backupGCS.getFileConfig().getMaxBackups();
                if (maxBackups > 0) {
                    TreeMap<Date, Blob> backups = new S3List(s3backupGCS).list(null, false, 0);

                    while (backups.size() > maxBackups) {
                        Date remove = backups.firstKey();
                        String toRemove = Helpers.formatListedBlobName(backups.get(remove), s3backupGCS.getFileConfig().getPrefix());
                        s3backupGCS.s3Delete().delete(player, toRemove);
                        backups.remove(remove);
                    }
                }
                new HealthCheckBuilder().success(s3backupGCS).runTaskAsynchronously(s3backupGCS);

            } catch (Exception e) {
                s3backupGCS.exception(player, "Backup failed", e);
                new HealthCheckBuilder().fail(s3backupGCS).runTaskAsynchronously(s3backupGCS);
            } finally {
                Bukkit.getScheduler().callSyncMethod(s3backupGCS, () -> Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-on"));
                s3backupGCS.setProgress(false);
            }
        }

    }
}
