package cloud.stonehouse.s3backupGCS;

public class HealthCheckBuilder {
    public HealthCheck success(s3backupGCS s3backupGCS) {
        return new HealthCheck(s3backupGCS, HealthCheck.HC_SUCCESS);
    }
    public HealthCheck fail(s3backupGCS s3backupGCS) {
        return new HealthCheck(s3backupGCS, HealthCheck.HC_FAIL);
    }
    public HealthCheck start(s3backupGCS s3backupGCS) {
        return new HealthCheck(s3backupGCS, HealthCheck.HC_START);
    }
}
