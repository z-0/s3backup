package cloud.stonehouse.s3backupGCS;

import org.bukkit.configuration.file.FileConfiguration;

public class Config {

    private final s3backupGCS s3backupGCS;
    private final String jsonKeyPath;
    private final String backupDir;
    private FileConfiguration config;
    private final String backupDateFormat;
    private final int backupInterval;
    private final String chatPrefix;
    private final String bucket;
    private final Boolean debug;
    private final String helpString;
    private final String[] ignoreFilesEndsWith;
    private final String[] ignoreFilesContains;
    private final int maxBackups;
    private final String prefix;
    private final String hcPingGuid;

    Config(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
        this.s3backupGCS.saveDefaultConfig();
        config = this.s3backupGCS.getConfig();

        backupDateFormat = config.getString("backup-date-format");
        backupDir = "backup";
        backupInterval = config.getInt("backup-interval");
        bucket = config.getString("bucket");
        chatPrefix = "§7[§es3backupGCS§7] ";
        debug = config.getBoolean("debug");
        // helpString = "/s3backupGCS [<backup <name>> <list>] [<get delete sign> <backup>]";
        helpString = "/s3backupGCS [<backup <name>> <list>] [<get delete> <backup>]";
        ignoreFilesEndsWith = config.getList("ignore-files-endswith").toArray(new String[0]);
        ignoreFilesContains = config.getList("ignore-files-contains").toArray(new String[0]);
        jsonKeyPath = config.getString("json-key-path");
        maxBackups = config.getInt("max-backups");
        prefix = checkTrailingSlashIfNotEmpty(config.getString("prefix"));
        hcPingGuid = config.getString("healthchecks.io-guid");
    }

    private String checkTrailingSlashIfNotEmpty(String string) {
        if (string == "" || string == null || string.trim().isEmpty()) return "";
        return string.endsWith("/") ? string : string + "/";
    }

    String getBackupDateFormat() {
        return backupDateFormat;
    }

    public String getBackupDir() {
        return backupDir;
    }

    int getBackupInterval() {
        return backupInterval;
    }

    public String getBucket() {
        return bucket;
    }

    String getChatPrefix() {
        return chatPrefix;
    }

    Boolean getDebug() {
        return debug;
    }

    String getHelpString() {
        return helpString;
    }

    String[] getIgnoreFilesEndsWith() {
        return ignoreFilesEndsWith;
    }

    String[] getIgnoreFilesContains() {
        return ignoreFilesContains;
    }

    int getMaxBackups() {
        return maxBackups;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getJsonKeyPath() {
        return jsonKeyPath;
    }

    public String getHcPingGuid() {
        return hcPingGuid;
    }
}
