package cloud.stonehouse.s3backupGCS;

import com.google.cloud.storage.Blob;

public class Helpers {

    public static String formatListedBlobName(Blob blob, String prefix) {
        return blob.getName()
        .replace(prefix, "")
        .replaceAll("^/", "");
    }
}
