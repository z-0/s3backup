package cloud.stonehouse.s3backupGCS;

import cloud.stonehouse.s3backupGCS.s3.S3Get;
import cloud.stonehouse.s3backupGCS.s3.S3List;
import com.google.cloud.storage.Blob;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.*;

class Commands3backupGCS implements TabExecutor {

    private final s3backupGCS s3backupGCS;
    private final String helpString;

    Commands3backupGCS(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
        this.helpString = s3backupGCS.getFileConfig().getHelpString();
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = null;

        if (commandSender instanceof Player) {
            player = (Player) commandSender;
        }

        if (args.length >= 1) {
            if (args[0].equalsIgnoreCase("backup")) {
                if (s3backupGCS.hasPermission(player, "s3backupGCS.backup")) {
                    if (args.length == 2) {
                        String name = args[1];
                        try {
                            if (s3backupGCS.illegalString(name)) {
                                throw new StringFormatException("Invalid backup name. Only alphanumeric characters, " +
                                        "underscores and hyphens are permitted");
                            } else {
                                new Backup(s3backupGCS, player, name + "-").runTaskAsynchronously(s3backupGCS);
                            }
                        } catch (StringFormatException e) {
                            s3backupGCS.exception(player, "Error", e);
                        }
                    } else {
                        new Backup(s3backupGCS, player, "manual-").runTaskAsynchronously(s3backupGCS);
                    }
                } else {
                    s3backupGCS.sendMessage(player, "You do not have permission for this command");
                }
            } else if (args[0].equalsIgnoreCase("list")) {
                if (s3backupGCS.hasPermission(player, "s3backupGCS.list")) {
                    int limit = 0;

                    if (args.length == 2) {
                        try {
                            limit = Integer.parseInt(args[1]);
                        } catch (NumberFormatException e) {
                            s3backupGCS.sendMessage(player, "Invalid number to list");
                            return false;
                        }
                    }
                    TreeMap<Date, Blob> backups = s3backupGCS.s3List().list(player, true, limit);
                    if (backups.size() == 0) {
                        s3backupGCS.sendMessage(player, "There are no backups to list");
                    }
                } else {
                    s3backupGCS.sendMessage(player, "You do not have permission for this command");
                }
            } else if (args[0].equalsIgnoreCase("delete")) {
                if (s3backupGCS.hasPermission(player, "s3backupGCS.delete")) {
                    if (args.length == 2) {
                        s3backupGCS.s3Delete().delete(player, args[1]);
                    } else {
                        s3backupGCS.sendMessage(player, helpString);
                    }
                } else {
                    s3backupGCS.sendMessage(player, "You do not have permission for this command");
                }
            // } else if (args[0].equalsIgnoreCase("sign")) {
            //     if (s3backupGCS.hasPermission(player, "s3backupGCS.sign")) {
            //         if (args.length == 2) {
            //             s3backupGCS.s3Sign().sign(player, args[1]);
            //         } else {
            //             s3backupGCS.sendMessage(player, helpString);
            //         }
            //     } else {
            //         s3backupGCS.sendMessage(player, "You do not have permission for this command");
            //     }
            } else if (args[0].equalsIgnoreCase("get")) {
                if (s3backupGCS.hasPermission(player, "s3backupGCS.get")) {
                    if (args.length == 2) {
                        new S3Get(s3backupGCS, player, args[1]).runTaskAsynchronously(s3backupGCS);
                    } else {
                        s3backupGCS.sendMessage(player, helpString);
                    }
                } else {
                    s3backupGCS.sendMessage(player, "You do not have permission for this command");
                }
            } else {
                s3backupGCS.sendMessage(player, helpString);
            }
        } else {
            s3backupGCS.sendMessage(player, helpString);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String s, String[] args) {
        Player player = null;
        List<String> types = new ArrayList<>();
        types.add("backup");
        types.add("delete");
        types.add("get");
        types.add("list");
        // types.add("sign");

        if (commandSender instanceof Player) {
            player = (Player) commandSender;
        }

        if (args.length == 1) {
            if (s3backupGCS.hasPermission(player, "s3backupGCS")) {
                List<String> result = new ArrayList<>();

                if (!args[0].equals("")) {
                    for (String type : types) {
                        if (type.toLowerCase().startsWith(args[0].toLowerCase())) {
                            result.add(type);
                        }
                    }
                    Collections.sort(result);
                    return result;
                } else {
                    Collections.sort(types);
                    return types;
                }
            }
        } else if (args.length == 2) {
            List<String> result = new ArrayList<>();

            if ((args[0].equalsIgnoreCase("get") &&
                    s3backupGCS.hasPermission(player, "s3backupGCS.get")) ||
                    (args[0].equalsIgnoreCase("delete") &&
                            s3backupGCS.hasPermission(player, "s3backupGCS.delete")) ||
                    (args[0].equalsIgnoreCase("sign") &&
                            s3backupGCS.hasPermission(player, "s3backupGCS.sign"))) {

                TreeMap<Date, Blob> backupMap = new S3List(s3backupGCS).list(null, false, 0);
                ArrayList<String> backups = new ArrayList<>();

                for (Blob backup : backupMap.values()) {
                    backups.add(Helpers.formatListedBlobName(backup, s3backupGCS.getFileConfig().getPrefix()));
                }

                if (!args[1].equals("")) {
                    for (String backup : backups) {
                        if (backup.toLowerCase().startsWith(args[1].toLowerCase())) {
                            result.add(backup);
                        }
                    }
                    Collections.sort(result);
                    return result;
                } else {
                    Collections.sort(backups);
                    return backups;
                }
            }
        }
        return null;
    }
}