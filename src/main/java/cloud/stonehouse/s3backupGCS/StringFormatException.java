package cloud.stonehouse.s3backupGCS;

class StringFormatException extends Exception {

    StringFormatException(String errorMessage) {
        super(errorMessage);
    }
}
