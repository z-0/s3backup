package cloud.stonehouse.s3backupGCS.s3;

import cloud.stonehouse.s3backupGCS.s3backupGCS;
import com.google.cloud.storage.Blob;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class S3Get extends BukkitRunnable {

    private final s3backupGCS s3backupGCS;
    private final String backup;
    private final Player player;

    public S3Get(s3backupGCS s3backupGCS, Player player, String backup) {
        this.s3backupGCS = s3backupGCS;
        this.backup = backup;
        this.player = player;
    }

    @Override
    public void run() {
        try {
            String filePrefix = s3backupGCS.getFileConfig().getPrefix() + backup;
            if (s3backupGCS.backupExists(filePrefix)) {
                Blob s3object = s3backupGCS.getClient().get(s3backupGCS.getFileConfig().getBucket(), filePrefix);
                s3backupGCS.sendMessage(player, "Started download of " + backup);

                Path targetPath = Paths.get(s3backupGCS.getFileConfig().getBackupDir(), File.separator, backup);
                s3object.downloadTo(targetPath);
                s3backupGCS.sendMessage(player, backup + " downloaded to the local backup directory");
            } else {
                s3backupGCS.sendMessage(player, "Backup " + backup + " does not exist");
            }
        } catch (Exception e) {
            s3backupGCS.exception(player, "Backup download failed", e);
        }
    }
}
