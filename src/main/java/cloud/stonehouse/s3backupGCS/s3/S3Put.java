package cloud.stonehouse.s3backupGCS.s3;

import cloud.stonehouse.s3backupGCS.s3backupGCS;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.google.cloud.WriteChannel;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;

public class S3Put {

    private final s3backupGCS s3backupGCS;

    public S3Put(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
    }

    public void put(String archiveName) throws IOException {
        Path targetFilePath = Paths.get(s3backupGCS.getFileConfig().getBackupDir() + File.separator + archiveName);
        String objectName = s3backupGCS.getFileConfig().getPrefix() + archiveName;
        BlobId blobId = BlobId.of(s3backupGCS.getFileConfig().getBucket(), objectName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        try (WriteChannel writer = s3backupGCS.getClient().writer(blobInfo)) {

        byte[] buffer = new byte[10_240];
        try (InputStream input = Files.newInputStream(targetFilePath)) {
            int limit;
            while ((limit = input.read(buffer)) >= 0) {
                writer.write(ByteBuffer.wrap(buffer, 0, limit));
            }
        }

    }
    }
}
