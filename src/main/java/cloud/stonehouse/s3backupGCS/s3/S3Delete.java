package cloud.stonehouse.s3backupGCS.s3;

import cloud.stonehouse.s3backupGCS.s3backupGCS;
import org.bukkit.entity.Player;

public class S3Delete {

    private final s3backupGCS s3backupGCS;

    public S3Delete(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
    }

    public void delete(Player player, String backup) {
        String filePrefix = s3backupGCS.getFileConfig().getPrefix() + backup;

        try {
            if (s3backupGCS.backupExists(filePrefix)) {
                s3backupGCS.getClient().delete(s3backupGCS.getFileConfig().getBucket(), filePrefix);
                s3backupGCS.sendMessage(player, "Backup " + backup + " has been deleted");
            } else {
                s3backupGCS.sendMessage(player, "Backup " + backup + " does not exist");
            }
        } catch (Exception e) {
            s3backupGCS.exception(player, "Backup delete failed", e);
        }
    }
}
