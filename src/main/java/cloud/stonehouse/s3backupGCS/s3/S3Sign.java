package cloud.stonehouse.s3backupGCS.s3;
// package cloud.stonehouse.s3backupGCS.s3;

// import cloud.stonehouse.s3backupGCS.s3backupGCS;
// import com.amazonaws.HttpMethod;
// import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
// import org.bukkit.entity.Player;

// import java.net.URL;
// import java.util.Date;

// public class S3Sign {

//     private final s3backupGCS s3backupGCS;

//     public S3Sign(s3backupGCS s3backupGCS) {
//         this.s3backupGCS = s3backupGCS;
//     }

//     public void sign(Player player, String backup) {
//         String filePrefix = s3backupGCS.getFileConfig().getPrefix() + backup;

//         if (s3backupGCS.backupExists(filePrefix)) {
//             try {
//                 Date expiration = new Date();
//                 long expire = expiration.getTime();
//                 expire += 1000 * 60 * s3backupGCS.getFileConfig().getSignedUrlDuration();
//                 expiration.setTime(expire);

//                 GeneratePresignedUrlRequest urlRequest =
//                         new GeneratePresignedUrlRequest(s3backupGCS.getFileConfig().getBucket(),
//                                 s3backupGCS.getFileConfig().getPrefix() + backup)
//                                 .withMethod(HttpMethod.GET)
//                                 .withExpiration(expiration);
//                 URL url = s3backupGCS.getClient().generatePresignedUrl(urlRequest);
//                 s3backupGCS.sendMessage(player, url.toString());
//             } catch (Exception e) {
//                 s3backupGCS.exception(player, "Error generating a signed URL", e);
//             }
//         } else {
//             s3backupGCS.sendMessage(player, "Backup " + backup + " does not exist");
//         }
//     }
// }
