package cloud.stonehouse.s3backupGCS.s3;

import cloud.stonehouse.s3backupGCS.s3backupGCS;

import java.io.FileInputStream;

import com.google.auth.Credentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;


public class S3Client {

    private final s3backupGCS s3backupGCS;
    private Storage client;

    public S3Client(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
        this.client = buildClient();
    }

    private Storage buildClient() {
        try {
            FileInputStream credentialsFileStream = new FileInputStream(s3backupGCS.getFileConfig().getJsonKeyPath());
            Credentials credentials = ServiceAccountCredentials.fromStream(credentialsFileStream);

            client = StorageOptions.newBuilder()
                .setCredentials(credentials)
                .build()
                .getService();

            return client;
        } catch (Exception e) {
            s3backupGCS.exception(null, "Failed to build s3 client", e);
            return null;
        }
    }

    public Storage getClient() {
        return client;
    }
}
