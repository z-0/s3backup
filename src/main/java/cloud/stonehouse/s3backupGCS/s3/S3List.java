package cloud.stonehouse.s3backupGCS.s3;

import cloud.stonehouse.s3backupGCS.Helpers;
import cloud.stonehouse.s3backupGCS.s3backupGCS;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage.BlobListOption;

import org.bukkit.entity.Player;

import java.util.Date;
import java.util.TreeMap;

public class S3List {

    private final s3backupGCS s3backupGCS;

    public S3List(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
    }

    public TreeMap<Date, Blob> list(Player player, boolean list, Integer limit) {
        String prefix = s3backupGCS.getFileConfig().getPrefix();
        String bucket = s3backupGCS.getFileConfig().getBucket();
        TreeMap<Date, Blob> backups = new TreeMap<>();

        try {
            Page<Blob> result;

            result = s3backupGCS.getClient().list(bucket, BlobListOption.prefix(prefix));

            for (Blob os : result.iterateAll()) {
                if (! os.getName().endsWith(".zip")) {
                    continue;
                }
                backups.put(new Date(os.getCreateTime()), os);
            }

            if (list) {
                int count = 0;
                int total = 0;
                long totalBackupSize = 0L;

                for (Blob backup : backups.values()) {
                    String name = Helpers.formatListedBlobName(backup, prefix);
                    String size = s3backupGCS.convertBytes(backup.getSize(), false);

                    if (limit > 0) {
                        if (count >= backups.size() - limit) {
                            s3backupGCS.sendMessage(player, name + " (" + size + ")");
                            totalBackupSize += backup.getSize();
                            total++;

                        }
                    } else {
                        s3backupGCS.sendMessage(player, name + " (" + size + ")");
                        totalBackupSize += backup.getSize();
                        total++;

                    }
                    count++;
                }
                s3backupGCS.sendMessage(player, "Total space used by " + total + " backups: " +
                        s3backupGCS.convertBytes(totalBackupSize, false));
            return backups;
            }
        } catch (Exception e) {
            s3backupGCS.exception(player, "Error retrieving backup list", e);
        }
        return backups;
    }
}
