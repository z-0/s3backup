package cloud.stonehouse.s3backupGCS;

import org.bukkit.scheduler.BukkitRunnable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class HealthCheck extends BukkitRunnable {
    public static final String HC_SUCCESS = "";
    public static final String HC_FAIL = "/fail";
    public static final String HC_START = "/start";
    private final String HEALTHCHECK_URL_BASE = "https://hc-ping.com/";

    private final s3backupGCS s3backupGCS;
    private Boolean sendHealthCheckRequest = true;
    private final String hcPingGuid;
    private final String hcType;

    HealthCheck(s3backupGCS s3backupGCS, String hcType) {
        this.s3backupGCS = s3backupGCS;
        hcPingGuid = s3backupGCS.getFileConfig().getHcPingGuid();
        this.hcType = hcType;
        if (hcPingGuid == null || hcPingGuid == "") {
            sendHealthCheckRequest = false;
        }
    }

    @Override
    public void run() {
        String healthCheckUrl = HEALTHCHECK_URL_BASE + hcPingGuid + hcType;
        if (sendHealthCheckRequest) {
            get(healthCheckUrl);
        }
    }

    private void get(String webUrl) {
        try {
            URL url = new URL(webUrl);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            in.close();
        }
        catch (java.io.IOException e1) {
            s3backupGCS.getLogger().warning("healthcheck.io:" + e1.getMessage());
        }
    
    }
}
