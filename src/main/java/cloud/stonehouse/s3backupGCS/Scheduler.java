package cloud.stonehouse.s3backupGCS;

import org.bukkit.scheduler.BukkitRunnable;

class Scheduler extends BukkitRunnable {

    private final s3backupGCS s3backupGCS;

    Scheduler(s3backupGCS s3backupGCS) {
        this.s3backupGCS = s3backupGCS;
        s3backupGCS.sendMessage(null, "Starting backup scheduler");
    }

    @Override
    public void run() {
        new Backup(s3backupGCS, null, "scheduled-").runTaskAsynchronously(s3backupGCS);
    }
}
