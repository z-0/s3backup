# s3backupGCS
Based on [Stephen Stonehouse's s3backup](https://gitlab.com/steve.stonehouse/s3backup).

A simple plugin for Spigot that uploads compressed backups of your server to Google Cloud Storage (GCS).

Also provides ways of downloading the backups for restoration.

## Requirements
 - An Google Cloud Platform Account.
 - An Google Cloud Storage Bucket.

The GCP resources used are not managed by this plugin. They can either be created through the console or via tools such as Terraform. This is to leave the GCP side open ended. 

t is recommended that default Bucket encryption at rest or via a Customer-managed encryption key (CMEK) be enabled for the bucket as plugin configurations and data can contain sensitive information.

 ### IAM Service Account credentials
 - An IAM Service Account user with JSON Key.
 - Grant the Service Account user permissions on the Bucket.

 ## [Healthchecks.io](https://healthchecks.io) Compatibility
This plugin also optionally integrates with Healthchecks.io to provide alerts for missed/failed backups.

The specified Healthchecks.io Check will be pinged on Backup success, also logging the duration of the backup.

The specified Healthchecks.io Check will be sent a failure ping if the backup fails.

To use this feature, provide the GUID (unique ID) part of your Check's Ping URL to the `healthchecks.io-guid` key of the Plugin configuration.

## Plugin configuration
This configuration will produce a `zip` backup of the server directory every 4 hours and upload it to your bucket with the path `my-backup-bucket/s3backupGCS/00-00-0000-00-00-00.zip`.

The backup process will also remove any old backups beyond the `max-backups`. This can be set to `0` if you would like to manage this yourself by other means.

File paths can be added to `ignore-files` if you do not want certain files to be contained in the backup. Useful for sensitive information. By default, the s3backupGCS configuration is not included, as it may contain AWS access keys.
```
################################################
#            s3backupGCS configuration            #
################################################

# See https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html for formatting options
# Note only hyphens and underscores are permitted alongside alphanumeric characters
backup-date-format: dd-MM-yyyy-HH-mm-ss

# Interval in minutes in which to create an automatic backup
backup-interval: 240

# Name of your s3 bucket
bucket: ''

# Path to Service Account JSON Key file
json-key-path: ''

# Enable this only for debugging. It will print stack traces to the console alongside normal error messages
debug: false

# List of files to omit from a backup. Useful for files with sensitive information
# These are matched in reverse. So a value of 'config.yml' will omit all config files found in the server root
# Note that servers running on Windows will need to use '\' for file separators
ignore-files-endswith:
  - s3backupGCS/config.yml
  - s3backupGCS\config.yml
ignore-files-contains:
  - rbackup_world

# Maximum backups in s3 before automatically deleting the oldest. Set to 0 to disable this
max-backups: 60

# Optional prefix to prepend to the backup in s3. Useful for having one bucket for multiple servers
# This is not visible to the plugin when listing/getting backups etc
# Forward slashes denote a folder in s3. So a value of 's3backupGCS/' will store all backups in a folder called s3backupGCS
prefix: ''

# Optional healthchecks.io GUID to send Healthchecks.io Pings on Backup completion/failure
# healthchecks.io-guid: ''

```
Backup names (including date format) and bucket prefix must only consist of alphanumeric characters, hyphens or underscores. The prefix can also contain forward slashes to denote folders in GCS (useful for seperating multi-server backups in one bucket).

## Commands
- `/s3backupGCS` - Displays command usage.
- `/s3backupGCS backup [name]` - Initiates a manual backup. Optional name to prepend.
- `/s3backupGCS delete [backup]` - Deletes the specified backup in GCS.
- `/s3backupGCS get [backup]` - Downloads the specified backup to the local `backup` directory.
- `/s3backupGCS list` - Lists the backups in GCS.

- ~~`/s3backupGCS sign [backup]` - Generates a temporary URL to download a backup locally.~~
    - `sign` is currently not supported

All commands auto-complete including the `get` and `delete` commands to fill in backup names.

## Permissions
- `s3backupGCS` - Allows displaying command usage. Granted by any other permission.
- `s3backupGCS.backup` - Allows creation of backups.
- `s3backupGCS.delete` - Allows deletion of backups.
- `s3backupGCS.get` - Allows downloading backups to the server backup directory.
- `s3backupGCS.list` - Allows the listing of backups.
- ~~`s3backupGCS.sign` - Allows downloading backups locally via generating a signed URL.~~
- `s3backupGCS.*` - Grants all of the above permissions.
